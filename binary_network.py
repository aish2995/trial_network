from typing import Union
from acme import specs
import sonnet as snt
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
import pandas as pd

tfd = tfp.distributions
tfb = tfp.bijectors

class BinaryOutcome(snt.Module):
  """Sonnet module squashing real-valued inputs to match a BoundedArrayDiscreteSpec."""

  def __init__(self, spec: specs.BoundedArray, name: str = 'bin_outcome'):
    super().__init__(name=name)
    self._scale = spec.maximum - spec.minimum
    self._offset = spec.minimum

  def __call__(
      self, inputs: Union[tf.Tensor, tfd.Distribution]
      ) -> Union[tf.Tensor, tfd.Distribution]:
    if isinstance(inputs, tfd.Distribution):
      inputs = tfb.Tanh()(inputs)
      inputs = tfb.ScaleMatvecDiag(0.5 * self._scale)(inputs)
      output = tfb.Shift(self._offset + 0.5 * self._scale)(inputs)
    else:
      inputs = tf.tanh(inputs)  # [-1, 1]
      inputs = 0.5 * (inputs + 1.0)  # [0, 1]
      output = inputs * self._scale + self._offset  # [minimum, maximum]
      output = tf.round(output)
      d = {'col1': [output]}
      df = pd.DataFrame(data=d, dtype=np.int32)
      output = df['col1'].values[0]	
    return output